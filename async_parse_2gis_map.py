import asyncio
from asyncio import sleep
import sys
import motor.motor_asyncio
import arsenic
from arsenic import get_session, keys, browsers, services
from arsenic.actions import Mouse, chain
from arsenic.session import Element, Session


SETTINGS = dict(
    latitude_begin=69.00,
    latitude_end=43.00,
    longitude_begin=28.00,
    longitude_end=180.00,
    search_word="Бетон",
    company_name="h1 span._oqoid",
    btn_phone="button._1vzogzt",
    phone_numbers="div._b0ke8",
    urls="div._49kxlr div a._hn9fqk",
    address_street_1='div[data-n="wat-address"] ul._y9ev9r li._er2xx9 a._2tqhrev',
    address_street_2='div[data-n="wat-address"] ul._y9ev9r li._er2xx9',
    address_city='div._1p8iqzw[data-n="wat-address-drilldown"]',
    emails='div._49kxlr div a._2tqhrev[target="_blank"]',
    companies_list="._d90rfj",
    arrow_next='div._1fbvw2b4[data-t="right"][data-s="true"]',
    arrow_next_disable='div._1fbvw2b4[data-t="right"][data-s="false"]',
    no_result='div._17qw31d h1._1hyxgif',
    input_place='input._1rs2vss',
)


class AsyncParse2GisMap():

    def __init__(self, search_word="Бетон"):
        client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
        db = client.companies_db
        self.company = db.two_gis_companies
        self.service = services.Chromedriver(binary='./chromedriver')
        self.browser = browsers.Chrome(
            chromeOptions={'args': ['--headless', '--disable-gpu']})
        self.search_word = search_word
        self.template_url = "https://beta.2gis.ru/search/{search_word}?lang=ru&m={longitude}%2C{latitude}%2F8"

    async def __move_and_click_btn(self, session, source_element, sec):
        mouse = Mouse()
        actions = chain(
            mouse.move_to(source_element),
            mouse.pause(sec),
            mouse.down(),
            mouse.up(),
        )
        await session.perform_actions(actions)

    async def __save_company(self, company):
        is_in_db = await self.company.find_one(company)
        if not is_in_db:
            await self.company.insert_one(company)

    async def __get_company_detail(self, session):
        def check_phone(num):
            if "\n" in num:
                num = num.split("\n")[0]
            return num

        company_name = await session.wait_for_element(3, SETTINGS["company_name"])
        company_name = await company_name.get_text()
        try:
            try:
                btn_phone = await session.get_element(SETTINGS["btn_phone"])
                await btn_phone.click()
            except:
                pass
            phone_numbers = await session.get_elements(SETTINGS["phone_numbers"])
            phone_number_list = [check_phone(await phone_number.get_text()) for phone_number in phone_numbers]
        except:
            phone_number_list = []
        try:
            urls = await session.get_elements(SETTINGS["urls"])
            url_list = [await url.get_text() for url in urls if "." in await url.get_text()]
        except:
            url_list = []
        try:
            address_street_1 = await session.get_element(SETTINGS["address_street_1"])
            address_street_1 = await address_street_1.get_text()
            try:
                address_street_2 = await session.get_element(SETTINGS["address_street_2"])
                address_street_2 = await address_street_2.get_text()
            except:
                address_street_2 = ""

            if address_street_1 == address_street_2:
                address_street = address_street_1
            else:
                address_street = f"{address_street_1} {address_street_2}"

            address_city = await session.get_element(SETTINGS["address_city"])
            address = "{}, {}".format(address_street, await address_city.get_text())
        except:
            address = ""
        try:
            emails = await session.get_elements(SETTINGS["emails"])
            email_list = [await email.get_text() for email in emails if "@" in await email.get_text()]
        except:
            email_list = []

        return {"name": company_name,
                "phone_number": phone_number_list,
                "url": url_list,
                "address": address,
                "email": email_list,
                }

    async def __scrolling_companies(self, session):
        await sleep(2)
        companies_list = await session.get_elements(SETTINGS["companies_list"])
        for company in companies_list:
            await self.__move_and_click_btn(session, company, 5)
            company = await self.__get_company_detail(session)
            await self.__save_company(company)

    async def __pagination(self, session):
        await self.__scrolling_companies(session)
        while True:
            try:
                arrow_next = await session.wait_for_element(5, SETTINGS["arrow_next"])
                await self.__move_and_click_btn(session, arrow_next, 1)
            except:
                break
            await self.__scrolling_companies(session)

    async def __check_page_by_company(self, session, num, latitude_begin, latitude_end, longitude_begin, longitude_end):
        def next_step(latitude, longitude):
            if longitude <= longitude_end:
                longitude += 7.6
            elif longitude >= longitude_end:
                longitude = longitude_begin
                if latitude > 69.2:
                    latitude -= 1.4
                elif 67.54 < latitude <= 69.2:
                    latitude -= 1.5
                elif 65.85 < latitude <= 67.54:
                    latitude -= 1.6
                elif 64.0 < latitude <= 65.85:
                    latitude -= 1.7
                elif 62.0 < latitude <= 64.0:
                    latitude -= 1.8
                elif 59.9 < latitude <= 62.0:
                    latitude -= 1.9
                elif 57.6 < latitude <= 59.9:
                    latitude -= 2.0
                elif 55.2 < latitude <= 57.6:
                    latitude -= 2.1
                elif 52.6 < latitude <= 55.2:
                    latitude -= 2.2
                elif 49.9 < latitude <= 52.6:
                    latitude -= 2.3
                elif 47.0 < latitude <= 49.9:
                    latitude -= 2.4
                elif 44.0 < latitude <= 47.0:
                    latitude -= 2.5
                elif 40.8 < latitude <= 44.0:
                    latitude -= 2.6
                else:
                    latitude -= 3.0
            return latitude, longitude

        last_point = await self.company.find_one({"last_point_{}".format(num): True})
        if last_point:
            latitude, longitude = last_point.get("coordinate")
        else:
            latitude, longitude = (latitude_begin, longitude_begin)

        if longitude > longitude_end:
            latitude, longitude = next_step(latitude, longitude)

        while latitude > latitude_end:

            query = {"last_point_{}".format(num): True}
            data = {"last_point_{}".format(num): True, "coordinate": [
                latitude, longitude]}

            await self.company.replace_one(query, data, upsert=True)

            url = self.template_url.format(longitude=str(longitude), latitude=str(
                latitude), search_word=str(self.search_word))

            try:
                await session.get(url)
                input_place = await session.wait_for_element(4, settings['input_place'])
            except:
                await session.get(url)

            try:
                no_result = await session.wait_for_element(4, settings['no_result'])
                latitude, longitude = next_step(latitude, longitude)
                continue
            except:
                await self.__pagination(session)
                latitude, longitude = next_step(latitude, longitude)

    async def start_parse(self, num=1, latitude_begin=69.00, latitude_end=43.00, longitude_begin=28.00, longitude_end=180.00):
        async with get_session(self.service, self.browser) as session:
            await session.set_window_size(1920, 1080, handle='current')
            await self.__check_page_by_company(session,
                                               num=num,
                                               latitude_begin=latitude_begin,
                                               latitude_end=latitude_end,
                                               longitude_begin=longitude_begin,
                                               longitude_end=longitude_end)


def _select_number_threads():
    import multiprocessing
    cpu_count = int(multiprocessing.cpu_count())
    if cpu_count > 4:
        threads = cpu_count - 2
    elif 1 < cpu_count <= 4:
        threads = cpu_count - 1
    else:
        threads = cpu_count
    return threads


def _do_tasks():
    step = _select_number_threads()
    latitude_begin = SETTINGS["latitude_begin"]
    latitude_end = SETTINGS["latitude_end"]
    len_latitude = latitude_begin - latitude_end
    step_for_thread = len_latitude/step
    tasks = []
    curr_latitude_begin = latitude_begin
    curr_latitude_end = latitude_begin - step_for_thread
    num = 1

    while curr_latitude_begin - step_for_thread > latitude_end and curr_latitude_end > latitude_end:
        two_gis_parser = AsyncParse2GisMap(search_word=SETTINGS["search_word"])
        params = (num, curr_latitude_begin, curr_latitude_end,
                  SETTINGS["longitude_begin"], SETTINGS["longitude_end"],)
        tasks.append(two_gis_parser.start_parse(*params))
        num += 1
        curr_latitude_begin = curr_latitude_end
        curr_latitude_end = curr_latitude_begin - step_for_thread
    return tasks


async def main():
    tasks = _do_tasks()
    await asyncio.gather(*tasks)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
